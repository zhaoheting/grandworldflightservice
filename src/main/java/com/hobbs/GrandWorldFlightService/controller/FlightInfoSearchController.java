package com.hobbs.GrandWorldFlightService.controller;

import com.hobbs.GrandWorldFlightService.model.FlightSearchRequest;
import com.hobbs.GrandWorldFlightService.model.FlightSearchResponse;
import com.hobbs.GrandWorldFlightService.service.FlightInfoSearchService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * TODO
 *
 * @author HeTing.Zhao
 * @since 2022/3/29
 */
@RestController
@RequestMapping("")
public class FlightInfoSearchController {

    private final FlightInfoSearchService flightInfoSearchService;

    public FlightInfoSearchController(FlightInfoSearchService flightInfoSearchService) {
        this.flightInfoSearchService = flightInfoSearchService;
    }

    @PostMapping("/flightInfo")
    public ResponseEntity<FlightSearchResponse> searchFlightInfo(@RequestBody FlightSearchRequest flightSearchRequest) {
        return ResponseEntity.ok().body(flightInfoSearchService.searchFlightInfo(flightSearchRequest));
    }
}
