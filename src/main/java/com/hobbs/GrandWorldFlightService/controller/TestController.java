package com.hobbs.GrandWorldFlightService.controller;

import com.hobbs.GrandWorldFlightService.service.ConfigService;
import com.hobbs.GrandWorldFlightService.service.ProducerService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * TODO
 *
 * @author HeTing.Zhao
 * @since 2022/3/12
 */
@RestController()
@RequestMapping("/config")
public class TestController {

    private final ConfigService configService;
    private final ProducerService producerService;

    public TestController(ConfigService configService, ProducerService producerService) {
        this.configService = configService;
        this.producerService = producerService;
    }

    @GetMapping("/produce")
    public void produce(){
        this.producerService.produce();
    }
}
