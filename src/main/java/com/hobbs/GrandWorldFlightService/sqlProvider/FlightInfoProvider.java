package com.hobbs.GrandWorldFlightService.sqlProvider;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

/**
 * TODO
 *
 * @author HeTing.Zhao
 * @since 2022/3/29
 */
public class FlightInfoProvider {

    public String findBySearchInfo(@Param("username") String username) {
        SQL sql = new SQL();
        sql.SELECT("flight_info_id, flight_number, departure_code, " +
                "destination_code, departure_time, arrive_time, flight_inventory, price, last_update_time")
                .FROM("flight_info")
                .WHERE("departure_code=#{departureCode}")
                .WHERE("destination_code=#{destinationCode}")
                .WHERE("departure_time=#{departureTime}");
        return sql.toString();
    }
}
