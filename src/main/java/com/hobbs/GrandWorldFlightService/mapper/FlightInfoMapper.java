package com.hobbs.GrandWorldFlightService.mapper;

import com.hobbs.GrandWorldFlightService.model.FlightInfo;
import com.hobbs.GrandWorldFlightService.sqlProvider.FlightInfoProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;

import java.sql.Timestamp;
import java.util.List;

/**
 * TODO
 *
 * @author HeTing.Zhao
 * @since 2022/3/29
 **/
public interface FlightInfoMapper {

    @SelectProvider(type = FlightInfoProvider.class, method = "findBySearchInfo")
    @Results(id = "flightInfoResultMap", value = {
            @Result(property = "flightInfoId",column = "flight_info_id"),
            @Result(property = "flightNumber",column ="flight_number"),
            @Result(property = "departureCode",column = "departure_code"),
            @Result(property = "destinationCode",column ="destination_code"),
            @Result(property = "departureTime",column = "departure_time"),
            @Result(property = "arriveTime",column = "arrive_time"),
            @Result(property = "flightInventory",column ="flight_inventory"),
            @Result(property = "price",column ="price"),
            @Result(property = "lastUpdateTime",column ="last_update_time")
    })
    List<FlightInfo> selectFlightInfo(String departureCode,String destinationCode,Timestamp departureTime);
}