package com.hobbs.GrandWorldFlightService.model;

import java.sql.Timestamp;
import java.util.List;

/**
 * Response.
 *
 * @author HeTing.Zhao
 * @since 2022/3/29
 */
public class FlightSearchResponse {
    private String departureCode;
    private String destinationCode;
    private Timestamp departureTime;
    private List<FlightInfo> flightInfoList;

    public String getDepartureCode() {
        return departureCode;
    }

    public void setDepartureCode(String departureCode) {
        this.departureCode = departureCode;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }

    public Timestamp getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Timestamp departureTime) {
        this.departureTime = departureTime;
    }

    public List<FlightInfo> getFlightInfoList() {
        return flightInfoList;
    }

    public void setFlightInfoList(List<FlightInfo> flightInfoList) {
        this.flightInfoList = flightInfoList;
    }
}
