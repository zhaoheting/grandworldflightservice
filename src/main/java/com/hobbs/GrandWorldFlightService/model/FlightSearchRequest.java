package com.hobbs.GrandWorldFlightService.model;

import java.sql.Timestamp;

/**
 * FLight Search entity.
 *
 * @author HeTing.Zhao
 * @since 2022/3/29
 */
public class FlightSearchRequest {
    private String departureCode;
    private String destinationCode;
    private Timestamp departureTime;

    public String getDepartureCode() {
        return departureCode;
    }

    public void setDepartureCode(String departureCode) {
        this.departureCode = departureCode;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }

    public Timestamp getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Timestamp departureTime) {
        this.departureTime = departureTime;
    }
}
