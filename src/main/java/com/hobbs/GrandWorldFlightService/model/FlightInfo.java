package com.hobbs.GrandWorldFlightService.model;

import java.sql.Timestamp;

/**
 * Entity of a flight ticket.
 *
 * @author HeTing.Zhao
 * @since 2022/3/29
 */
public class FlightInfo {
    private int flightInfoId;
    private String flightNumber;
    private int price;
    private String departureCode;
    private String destinationCode;
    private Timestamp departureTime;
    private Timestamp arriveTime;
    private int flightInventory;
    private Timestamp lastUpdateTime;

    public int getFlightInfoId() {
        return flightInfoId;
    }

    public void setFlightInfoId(int flightInfoId) {
        this.flightInfoId = flightInfoId;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDepartureCode() {
        return departureCode;
    }

    public void setDepartureCode(String departureCode) {
        this.departureCode = departureCode;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }

    public Timestamp getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Timestamp departureTime) {
        this.departureTime = departureTime;
    }

    public Timestamp getArriveTime() {
        return arriveTime;
    }

    public void setArriveTime(Timestamp arriveTime) {
        this.arriveTime = arriveTime;
    }

    public int getFlightInventory() {
        return flightInventory;
    }

    public void setFlightInventory(int flightInventory) {
        this.flightInventory = flightInventory;
    }

    public Timestamp getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Timestamp lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }
}
