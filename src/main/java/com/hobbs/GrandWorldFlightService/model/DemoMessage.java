package com.hobbs.GrandWorldFlightService.model;

import java.io.Serializable;
import java.util.Date;

/**
 * TODO
 *
 * @author HeTing.Zhao
 * @since 2022/3/31
 */
public class DemoMessage implements Serializable {

    private static final long serialVersionUID = -8013965441896177936L;

    public String url;
    public String content;
    public Date date;

    public DemoMessage(String url, String content, Date date) {
        this.url = url;
        this.content = content;
        this.date = date;
    }

    public DemoMessage() {
    }
}
