package com.hobbs.GrandWorldFlightService.beans;

import com.hobbs.cache.generator.AbstractKeyGenerator;
import org.apache.logging.log4j.util.Strings;

import java.sql.Timestamp;

/**
 * TODO
 *
 * @author HeTing.Zhao
 * @since 2022/3/28
 */
public class FlightKeyGenerator extends AbstractKeyGenerator<String> {

    private final static String CACHE_NAME = "CacheKey-Flight:";

    private String departureCode;
    private String destinationCode;
    private Timestamp departureTime;
    private String flightNumber;

    public FlightKeyGenerator(FlightKeyGenerator flightKeyGenerator, String flightNumber) {
        this(flightKeyGenerator.getDepartureCode(), flightKeyGenerator.getDestinationCode(), flightKeyGenerator.getDepartureTime(), flightNumber);
    }

    public FlightKeyGenerator(String departureCode, String destinationCode, Timestamp departureTime) {
        this(departureCode, destinationCode, departureTime, "");
    }

    public FlightKeyGenerator(String departureCode, String destinationCode, Timestamp departureTime, String flightNumber) {
        this.departureCode = departureCode;
        this.destinationCode = destinationCode;
        this.departureTime = departureTime;
        this.flightNumber = flightNumber;
        super.set("departureCode", this.departureCode);
        super.set("destinationCode", this.destinationCode);
        super.set("departureTime", this.departureTime);
        super.set("flightNumber", this.flightNumber);
    }

    @Override
    protected String generateKey() {
        StringBuilder key = new StringBuilder(CACHE_NAME).append(this.departureCode).append(this.destinationCode).append(this.departureTime);
        /**
         * When build key generator for fuzzy query, the flight number is empty.
         *
         * @return {@link String}
         */
        if (!Strings.isBlank(this.flightNumber)) {
            key.append(this.flightNumber);
        }
        return key.toString();
    }

    public static String getCacheName() {
        return CACHE_NAME;
    }

    public String getDepartureCode() {
        return departureCode;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public Timestamp getDepartureTime() {
        return departureTime;
    }

    public String getFlightNumber() {
        return flightNumber;
    }
}
