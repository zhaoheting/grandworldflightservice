package com.hobbs.GrandWorldFlightService.dataProvider;

import com.hobbs.GrandWorldFlightService.beans.FlightKeyGenerator;
import com.hobbs.GrandWorldFlightService.model.FlightInfo;
import com.hobbs.cache.dataProvider.DataProvider;
import com.hobbs.cache.decorator.AbstractDataProvider;
import com.hobbs.cache.decorator.RedisDataProvider;
import com.hobbs.cache.generator.KeyGenerator;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * This class provides a few customized methods for flight search scenario.
 * It is a decorator of {@link RedisDataProvider}
 * @author HeTing.Zhao
 * @since 2022/3/29
 */
public class FlightInfoRedisDataProvider extends AbstractDataProvider<String, FlightInfo> {
    private final RedisTemplate<String, FlightInfo> redisTemplate;

    public FlightInfoRedisDataProvider(DataProvider<String, FlightInfo> dataProvider, RedisTemplate<String, FlightInfo> redisTemplate) {
        super(dataProvider);
        Assert.notNull(redisTemplate, "Redis template cannot be null.");
        this.redisTemplate = redisTemplate;
    }

    /**
     * Get a flight info list by a single key.
     *
     * @param keyGenerator
     * @return {@link List< FlightInfo>}
     */
    public Optional<List<FlightInfo>> fuzzyQuery(KeyGenerator<String> keyGenerator) {
        return Optional.ofNullable(keyGenerator).map(KeyGenerator::getKey).flatMap(
                key -> {
                    Set<String> keySet = redisTemplate.keys(key + "*");
                    List<FlightInfo> flightInfoList = new ArrayList<>();
                    if (!CollectionUtils.isEmpty(keySet)) {
                        keySet.forEach(k -> {
                            flightInfoList.add(this.redisTemplate.opsForValue().get(k));
                        });
                    }
                    if (flightInfoList.isEmpty()) {
                        Optional<List<FlightInfo>> optionalFlightInfoList = this.dataProvider.fuzzyQuery(keyGenerator);
                        optionalFlightInfoList.ifPresent(list -> {
                            Map<FlightKeyGenerator, FlightInfo> map = new HashMap<>();
                            for (FlightInfo flightInfo : list) {
                                map.put(new FlightKeyGenerator(flightInfo.getDepartureCode(),
                                        flightInfo.getDestinationCode(), flightInfo.getDepartureTime(), flightInfo.getFlightNumber()), flightInfo);
                            }
                            this.dataProvider.multiSet(map);
                        });
                        return optionalFlightInfoList;
                    }
                    return Optional.of(flightInfoList);
                });
    }
}
