package com.hobbs.GrandWorldFlightService.dataProvider;

import com.hobbs.cache.dataProvider.AbstractReadOnlyDataProvider;
import com.hobbs.cache.generator.KeyGenerator;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class DbDataProvider<K, V> extends AbstractReadOnlyDataProvider<K, V> {

    public DbDataProvider(String cacheName) {
        super(cacheName);
    }

    @Override
    public Optional<V> get(KeyGenerator<K> key) {
        return Optional.empty();
    }

    @Override
    public Map<K, V> multiGet(Collection<? extends KeyGenerator<K>> keys) {
        return null;
    }

    @Override
    public Optional<List<V>> fuzzyQuery(KeyGenerator<K> keyGenerator) {
        return Optional.empty();
    }
}
