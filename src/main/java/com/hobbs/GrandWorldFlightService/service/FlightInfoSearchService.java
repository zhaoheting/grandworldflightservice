package com.hobbs.GrandWorldFlightService.service;

import com.hobbs.GrandWorldFlightService.mapper.FlightInfoMapper;
import com.hobbs.GrandWorldFlightService.model.FlightInfo;
import com.hobbs.GrandWorldFlightService.model.FlightSearchRequest;
import com.hobbs.GrandWorldFlightService.model.FlightSearchResponse;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * TODO
 *
 * @author HeTing.Zhao
 * @since 2022/3/29
 */
@Service
public class FlightInfoSearchService {
    private final FlightInfoMapper flightInfoMapper;
    private final FlightInfoCacheService flightInfoCacheService;

    public FlightInfoSearchService(FlightInfoMapper flightInfoMapper, FlightInfoCacheService flightInfoCacheService) {
        this.flightInfoMapper = flightInfoMapper;
        this.flightInfoCacheService = flightInfoCacheService;
    }

    public FlightSearchResponse searchFlightInfo(FlightSearchRequest flightSearchRequest) {
        List<FlightInfo> flightInfoList = flightInfoMapper.selectFlightInfo(flightSearchRequest.getDepartureCode(),
                flightSearchRequest.getDestinationCode(), flightSearchRequest.getDepartureTime());
        FlightSearchResponse flightSearchResponse = new FlightSearchResponse();
        flightSearchResponse.setFlightInfoList(flightInfoList);
        return flightSearchResponse;
    }
}
