package com.hobbs.GrandWorldFlightService.service;

import com.hobbs.GrandWorldFlightService.model.DemoMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;

/**
 * TODO
 *
 * @author HeTing.Zhao
 * @since 2022/3/31
 */
@Service
public class ConsumerService {

    Logger logger = LoggerFactory.getLogger(ConsumerService.class);

    @JmsListener(destination = "grand-world-flight-queue")
    public void consume(@Payload final Message<DemoMessage> message) throws JMSException {
        logger.info("Received: {}", message.getPayload().content);

    }
}
