package com.hobbs.GrandWorldFlightService.service;

import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

/**
 * All the properties are obtained from nacos.
 *
 * @author HeTing.Zhao
 * @since 2022/3/12
 */
@RefreshScope
@Service
public class ConfigService {

}
