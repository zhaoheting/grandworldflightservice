package com.hobbs.GrandWorldFlightService.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hobbs.GrandWorldFlightService.model.DemoMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.jms.Message;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * TODO
 *
 * @author HeTing.Zhao
 * @since 2022/3/31
 */
@Service
public class ProducerService {

    private final Logger LOGGER = LoggerFactory.getLogger(ProducerService.class);

    @Autowired
    protected JmsTemplate defaultJmsTemplate;

    ObjectMapper objectMapper = new ObjectMapper();

    public void sendToTestQueue(DemoMessage message) {
        sendToSQS("grand-world-flight-queue", message);
    }

    /**
     * Send message to a queue.
     * TODO Investigate how to use message creator.
     *
     * @param queue
     * @param payload
     * @param <T>
     */
    public <T extends Serializable> void sendToSQS(String queue, T payload) {
        LOGGER.info("Sending message {} to queue {}", payload, queue);

        defaultJmsTemplate.send(queue, session -> {
            try {
                Message createMessage = session.createTextMessage(objectMapper.writeValueAsString(payload));
                //I am not sure about the usages of these two properties. They make no effects on producing or consuming result.
//                createMessage.setStringProperty(SQSMessagingClientConstants.JMSX_GROUP_ID, "messageGroup1");
//                createMessage.setStringProperty(SQSMessagingClientConstants.JMS_SQS_DEDUPLICATION_ID, "2019" + System.currentTimeMillis());
                createMessage.setStringProperty("documentType", payload.getClass().getName());
                return createMessage;
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                LOGGER.error("Fail to send message {} ,err {}", payload, e.getMessage());
                throw new RuntimeException(e);
            }
        });
    }

    public void produce() {
        DemoMessage demoMessage = new DemoMessage(UUID.randomUUID().toString(), "Test queue", new Date());
        sendToTestQueue(demoMessage);
    }
}
