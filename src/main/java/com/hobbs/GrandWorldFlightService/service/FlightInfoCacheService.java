package com.hobbs.GrandWorldFlightService.service;

import com.hobbs.GrandWorldFlightService.beans.FlightKeyGenerator;
import com.hobbs.GrandWorldFlightService.dataProvider.DbDataProvider;
import com.hobbs.GrandWorldFlightService.dataProvider.FlightInfoRedisDataProvider;
import com.hobbs.GrandWorldFlightService.model.FlightInfo;
import com.hobbs.cache.config.DistributedCacheConfigProperties;
import com.hobbs.cache.decorator.DistributedCache;
import com.hobbs.cache.decorator.RedisDataProvider;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * TODO
 *
 * @author HeTing.Zhao
 * @since 2022/3/28
 */
@Service
public class FlightInfoCacheService {

    private final static String FLIGHT = "Flight";
    private final DistributedCache<String, FlightInfo> distributedCache;
    private final RedisTemplate<String, FlightInfo> redisTemplate;

    public FlightInfoCacheService(RedisTemplate<String, FlightInfo> redisTemplate, DistributedCacheConfigProperties distributedCacheConfigProperties) {
        this.distributedCache = new DistributedCache<>(
                new FlightInfoRedisDataProvider(
                        new RedisDataProvider<>(redisTemplate, new DbDataProvider<>("Cache:" + FLIGHT), distributedCacheConfigProperties), redisTemplate),
                "Cache:" + FLIGHT);
        this.redisTemplate = redisTemplate;
    }

    /**
     * TODO The algorithm need to be redesigned.
     *
     * @param keyGenerator
     * @return {@link List< FlightInfo>}
     */
    public List<FlightInfo> getAllFlightInfo(FlightKeyGenerator keyGenerator) {
        String key = keyGenerator.getKey();
        int keyLen = key.length();
        Set<String> keySet = this.redisTemplate.keys(key + "*");
        if (!CollectionUtils.isEmpty(keySet)) {
            Set<FlightKeyGenerator> newFlightKeyGeneratorSet = keySet.stream().map(k -> new FlightKeyGenerator(keyGenerator, k.substring(keyLen))).collect(Collectors.toSet());
            Map<String, FlightInfo> map = distributedCache.multiGet(newFlightKeyGeneratorSet);
            return new ArrayList<>(map.values());
        }
        return Collections.emptyList();
    }
}
