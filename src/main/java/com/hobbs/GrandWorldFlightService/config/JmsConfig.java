package com.hobbs.GrandWorldFlightService.config;

import com.amazon.sqs.javamessaging.ProviderConfiguration;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.jms.support.destination.DynamicDestinationResolver;

import javax.jms.Session;

/**
 * TODO
 *
 * @author HeTing.Zhao
 * @since 2022/3/31
 */
@Configuration
@EnableJms
public class JmsConfig {

    private final AmazonSQSAsyncClientBuilder amazonSQSAsyncClientBuilder;
    private final SQSConnectionFactory connectionFactory;

    public JmsConfig(@Value("${cloud.aws.credentials.access-key}") String awsAccessKey,
                     @Value("${cloud.aws.credentials.secret-key}") String awsSecretKey,
                     @Value("${cloud.aws.region}") String awsRegion,
                     @Value("${cloud.aws.endpoint.uri}") String endPoint) {
        amazonSQSAsyncClientBuilder = AmazonSQSAsyncClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsAccessKey, awsSecretKey)))
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endPoint, awsRegion));
        connectionFactory = new SQSConnectionFactory(new ProviderConfiguration(), this.amazonSQSAsyncClientBuilder.build());

    }

    /**
     * Format
     *
     * @return
     */
    @Bean
    public MessageConverter messageConverter() {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
        builder.serializationInclusion(JsonInclude.Include.NON_EMPTY);
        builder.dateFormat(new StdDateFormat());
        org.springframework.jms.support.converter.MappingJackson2MessageConverter mappingJackson2MessageConverter = new MappingJackson2MessageConverter();
        mappingJackson2MessageConverter.setObjectMapper(builder.build());
        mappingJackson2MessageConverter.setTargetType(MessageType.TEXT);
        mappingJackson2MessageConverter.setTypeIdPropertyName("documentType");
        return mappingJackson2MessageConverter;
    }

    @Bean
    public DefaultJmsListenerContainerFactory jmsListenerContainerFactory() {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(this.connectionFactory);
        factory.setDestinationResolver(new DynamicDestinationResolver());
        //The values we provided to Concurrency show that we will create a minimum of 3 listeners that will scale up to 10 listeners
        factory.setConcurrency("3-10");

        //How to confirm reliability.
        /**
         *  https://www.cnblogs.com/progor/p/10877658.html
         *
         *  SESSION_TRANSACTED
         *      Confirm when transaction is committed.
         *  CLIENT_ACKNOWLEDGE
         *      The listener must call method "acknowledge()" of "javax.jms.Message" to tell SQS to delete the consumed message.
         *  AUTO_ACKNOWLEDGE
         *      Confirm automatically. Consumer and producer do not some extra actions.
         *  DUPS_OK_ACKNOWLEDGE
         *      It is used when the message can be consumed repeatedly.
         */
        factory.setSessionAcknowledgeMode(Session.CLIENT_ACKNOWLEDGE);
        factory.setMessageConverter(messageConverter());
        return factory;
    }

    @Bean
    public JmsTemplate defaultJmsTemplate() {
        return new JmsTemplate(this.connectionFactory);
    }

}
